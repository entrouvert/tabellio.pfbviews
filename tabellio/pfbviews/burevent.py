import os

from five import grok
from zope import schema, component
from Products.CMFCore.utils import getToolByName

from plone.registry.interfaces import IRegistry
from tabellio.config.interfaces import ITabellioSettings

import tabellio.agenda.burevent


class BurEventBaseView(tabellio.agenda.parlevent.ParlEventBaseView):
    def has_stream(self):
        return False


class BurEventView(BurEventBaseView):
    def get_stream_name(self):
        return None


class View(grok.View, BurEventView):
    grok.context(tabellio.agenda.burevent.IBurEvent)
    grok.require('zope2.View')

