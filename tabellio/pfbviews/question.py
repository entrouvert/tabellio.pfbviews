from Products.Five import BrowserView

class View(BrowserView):
    def related_docs(self):
        if not self.context.related_docs:
            return []
        objects = [x.to_object for x in self.context.related_docs]
        return sorted(objects, lambda x,y: cmp(x.reftitle, y.reftitle))
