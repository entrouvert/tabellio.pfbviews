import os

from five import grok
from zope import schema, component
from Products.CMFCore.utils import getToolByName

from plone.registry.interfaces import IRegistry
from tabellio.config.interfaces import ITabellioSettings

import tabellio.agenda.parlevent


class ParlEventBaseView(tabellio.agenda.parlevent.ParlEventBaseView):
    def has_stream(self):
        settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        if not settings.audiofiles_path:
            return False
        return os.path.exists(os.path.join(settings.audiofiles_path, self.get_stream_name()))


class ParlEventView(ParlEventBaseView):
    def get_stream_name(self):
        settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        if settings.audiofiles_path:
            if os.path.exists(os.path.join(settings.audiofiles_path, self.context.id + '.mp3')):
                return self.context.id + '.mp3'
        filename = '%04d%02d%02d%02d-SEAN.mp3' % (
                        self.context.start.year, self.context.start.month,
                        self.context.start.day, self.context.start.hour)
        return filename


class View(grok.View, ParlEventView):
    grok.context(tabellio.agenda.parlevent.IParlEvent)
    grok.require('zope2.View')

class M3U(grok.View, ParlEventView):
    grok.context(tabellio.agenda.parlevent.IParlEvent)
    grok.require('zope2.View')
    grok.name('seance.m3u')

    def render(self):
        portal_url = getToolByName(self.context, 'portal_url').getPortalObject().absolute_url()
        self.request.response.setHeader('Content-type', 'audio/x-mpegurl')
        return portal_url + '/mp3/' + self.get_stream_name() + '\n'
