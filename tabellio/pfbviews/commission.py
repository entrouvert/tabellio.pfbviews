# -*- coding: utf-8 -*-

from zope import component
from Products.Five import BrowserView
from Products.CMFCore.utils import getToolByName
from plone.registry.interfaces import IRegistry

from themis.fields.vocabs import cmp_person
from tabellio.config.interfaces import ITabellioSettings

class View(BrowserView):
    def presidence_polgroups(self):
        return [self.context.president.to_object.polgroup.to_object]

    def presidence_members(self, polgroup):
        return [self.context.president.to_object]

    def list_polgroups(self, list):
        settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        current = getToolByName(self.context, 'portal_url').getPortalObject()
        for part in settings.polgroupsPath.split('/'):
            if not part:
                continue
            current = getattr(current, part)
        polgroup_ids = current.objectIds()

        items = []
        for member in list:
            polgroup = member.to_object.polgroup.to_object
            polgroup_id = polgroup.id
            items.append(polgroup_id)

        return [getattr(current, x) for x in polgroup_ids if x in items]

    def vicepresidence_polgroups(self):
        return self.list_polgroups(self.context.vicepresidents)

    def vicepresidence_members(self, polgroup):
        return sorted([x.to_object for x in self.context.vicepresidents
                       if x.to_object.polgroup.to_object == polgroup], cmp_person)

    def members_polgroups(self):
        return self.list_polgroups(self.context.members)

    def members_members(self, polgroup):
        return sorted([x.to_object for x in self.context.members
                       if x.to_object.polgroup.to_object == polgroup], cmp_person)

    def substitutes_polgroups(self):
        return self.list_polgroups(self.context.substitutes)

    def substitutes_members(self, polgroup):
        return sorted([x.to_object for x in self.context.substitutes
                       if x.to_object.polgroup.to_object == polgroup], cmp_person)
