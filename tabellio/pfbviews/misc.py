from Products.Five import BrowserView
from zope.interface import Interface
from Products.CMFCore.utils import getToolByName

try:
    from plone.app.caching.operations.utils import setCacheHeaders
except ImportError:
    setCacheHeaders = None

from tabellio.webviews.misc import PcfHomeFolder, IPcfHomeFolder

class Cached:
    def cache(self, duration=30):
        if setCacheHeaders:
            setCacheHeaders(self, self.request, self.request.response,
                        maxage=duration, smaxage=duration)

class IPfbHomeFolder(IPcfHomeFolder):
    pass

class PfbHomeFolder(PcfHomeFolder):
    def slides_js(self):
        portal = getToolByName(self.context, 'portal_url').getPortalObject()
        images = getattr(portal, 'images-accueil').objectIds()
        # move the first image to the last place, as it will be added statically
        images = images[1:] + [images[0]]
        s = '''SLIDES = new slideshow("SLIDES");\n'''
        s += '\n'.join(['''
SLIDES.add_slide(s = new slide('/images-accueil/%s/image'));
s.filter = 'progid:DXImageTransform.Microsoft.Fade()';''' % x for x in images])
        return s

    def firstpic_url(self):
        portal = getToolByName(self.context, 'portal_url').getPortalObject()
        images = getattr(portal, 'images-accueil').objectIds()
        return '/images-accueil/%s/image' % images[0]
