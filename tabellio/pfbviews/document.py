from Products.Five import BrowserView

class View(BrowserView):
    def related_docs(self):
        if not self.context.related_docs:
            return []
        objects = [x.to_object for x in self.context.related_docs if x.to_object]
        return sorted(objects, lambda x,y: cmp(x.title, y.title))

class DocumentDownload(BrowserView):
    def __call__(self):
        self.request.response.setHeader('Content-type',
                       self.context.file.contentType)
        return self.context.file.data
