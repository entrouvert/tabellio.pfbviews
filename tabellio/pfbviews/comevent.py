import os

from five import grok
from zope import schema, component
from Products.CMFCore.utils import getToolByName
from parlevent import ParlEventBaseView
from tabellio.agenda.comevent import IComEvent
import tabellio.config.utils
from plone.registry.interfaces import IRegistry
from tabellio.config.interfaces import ITabellioSettings

class CommissionView(ParlEventBaseView):
    def get_stream_name(self):
        settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        if settings.audiofiles_path:
            if os.path.exists(os.path.join(settings.audiofiles_path, self.context.id + '.mp3')):
                return self.context.id + '.mp3'
        if not self.context.commission:
            return 'XXXX'
        com_id = self.context.commission.to_path.split('/')[-1]
        com_audio_code = tabellio.config.utils.get_com_audio_code(com_id)
        filename = '%04d%02d%02d%02d-%s.mp3' % (
                        self.context.start.year, self.context.start.month,
                        self.context.start.day, self.context.start.hour,
                        com_audio_code)
        return filename

class View(grok.View, CommissionView):
    grok.context(IComEvent)
    grok.require('zope2.View')

class M3U(grok.View, CommissionView):
    grok.context(IComEvent)
    grok.require('zope2.View')
    grok.name('ecouter.m3u')

    def render(self):
        portal_url = getToolByName(self.context, 'portal_url').getPortalObject().absolute_url()
        self.request.response.setHeader('Content-type', 'audio/x-mpegurl')
        return portal_url + '/mp3/' + self.get_stream_name() + '\n'

