from zope.interface import Interface

def initialize(context):
    """Initializer called when used as a Zope 2 product."""

class IHemicycleDocument(Interface):
    pass

import zope.i18nmessageid
MessageFactory = zope.i18nmessageid.MessageFactory('tabellio.pfbviews')
