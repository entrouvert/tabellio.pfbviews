from Products.Five import BrowserView

class View(BrowserView):
    def deputies(self):
        return [x for x in self.context.getParentNode().objectValues()
                                if x.Type() == 'Deputy' and x.active and x.seat_number]

